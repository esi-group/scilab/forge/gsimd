/*
 * context.hpp
 *
 *  Created on: 24 juin 2010
 *      Author: vlj
 */

#ifndef CONTEXT_CUDA_HPP_
#define CONTEXT_CUDA_HPP_

/*!
 * \cond
 */

#include <vector>
#include <map>
using namespace std;

template<>
  inline
  int
  Context<CUDAmode>::number_of_device()
  {
    int deviceCount;
    __check_sanity__<CUDAmode> (cuDeviceGetCount(&deviceCount));
    return deviceCount;
  }

template<>
  inline
  Context<CUDAmode>::Context()
  {
    __check_sanity__<CUDAmode> (cuInit(0));
    for (int ordinal = 0; ordinal < number_of_device(); ordinal++)
      {
        devices_list.push_back(Device<CUDAmode> (ordinal));
      }

  }

template<>
  template<bool isGL>
    inline
    void
    Context<CUDAmode>::set_current_device(const Device<CUDAmode>& device)
    {
      if (isGL)
        {
          __check_sanity__<CUDAmode> (cuGLCtxCreate(&cont, 0, device.dev));
        }
      else
        {
          __check_sanity__<CUDAmode> (cuCtxCreate(&cont, 0, device.dev));
        }
      current_device = device;

    }

template<>
  inline
  Context<CUDAmode>::~Context()
  {
    loadedModule.clear();
    __check_sanity__ <CUDAmode> (
    cuCtxDestroy(cont));
  }

/*!
 * \endcond
 */

#endif /* CONTEXT_HPP_ */
