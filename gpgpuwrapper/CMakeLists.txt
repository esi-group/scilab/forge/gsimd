cmake_minimum_required(VERSION 2.8)

PROJECT(gpgpuwrapper)
ENABLE_TESTING()

find_package(CUDA REQUIRED)


IF(CMAKE_SIZEOF_VOID_P EQUAL 4)
    SET(LIB_SUFFIX "")
  ELSE(CMAKE_SIZEOF_VOID_P EQUAL 4)
    SET(LIB_SUFFIX 64)
ENDIF(CMAKE_SIZEOF_VOID_P EQUAL 4)


include_directories(include
${CUDA_INCLUDE_DIRS}
)

add_executable(builder
	src/builder.cpp
)
 
add_executable(test_app
	tests/main.cpp
)

target_link_libraries(builder
	pthread
	OpenCL
)

CUDA_ADD_CUFFT_TO_TARGET(test_app)
 
target_link_libraries(test_app
	pthread
	cuda
	OpenCL
	GLEW
	glut
	boost_filesystem-mt
 )

ADD_TEST(cuda_test test_app "1")
ADD_TEST(opencl_test test_app "2")
ADD_TEST(cufft_interop test_app "5")
ADD_TEST(cudamem test_app "6")
ADD_TEST(openclmem test_app "7")
