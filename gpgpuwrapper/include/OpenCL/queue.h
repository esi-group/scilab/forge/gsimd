/*
 * queue.h
 *
 *  Created on: 30 juin 2010
 *      Author: vlj
 */

#ifndef QUEUE_OPENCL_HPP_
#define QUEUE_OPENCL_HPP_

template<>
inline Queue<OPENCLmode>::Queue()
{

}



template<>
inline Queue<OPENCLmode>::Queue(Context_Handle c,Device_Handle d):cont(c),dev(d)
{
	cl_int ciErrNum = CL_SUCCESS;
	stream=clCreateCommandQueue(cont,dev,0,&ciErrNum);
	__check_sanity__<OPENCLmode>(ciErrNum);
}

#endif /* QUEUE_H_ */
