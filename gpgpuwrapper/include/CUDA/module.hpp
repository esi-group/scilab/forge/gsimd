/*
 * module.h
 *
 *  Created on: Jun 7, 2010
 *      Author: vlj
 */

#ifndef MODULE_H_
#define MODULE_H_



template<>
inline Module<CUDAmode>::Module(string f,Context_Handle,Device_Handle):isloaded(false),filename(f)
{

}

template<>
inline void Module<CUDAmode>::load()
{
	__check_sanity__<CUDAmode>( cuModuleLoad ( &mod,filename.c_str() ) );
	isloaded=true;
}

template<>
inline Module<CUDAmode>::Module():isloaded(false),filename("")
{

}

template<>
inline Module<CUDAmode>::Module(const Module& input):isloaded(false),filename(input.filename)
{
	if(input.isloaded)
		load();
}



template<>
inline Kernel<CUDAmode>& Module<CUDAmode>::getFunction(string kernelname) const
{
	if (storedfonc.find(kernelname) == storedfonc.end())
	{
		CUfunction tmp;
		__check_sanity__<CUDAmode>(cuModuleGetFunction (&tmp,mod,kernelname.c_str() ) );
		Kernel<CUDAmode> p(tmp);
		const_cast<Module&>(*this).storedfonc[kernelname] = p;

	}
	return const_cast<Module&>(*this).storedfonc[kernelname];
}

template<>
inline Module<CUDAmode>::~Module()
{
	if(isloaded)
		__check_sanity__<CUDAmode>( cuModuleUnload(mod) );
}


#endif /* MODULE_H_ */
