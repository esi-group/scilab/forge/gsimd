extern "C" {

__global__ void add_float(float* b,float* a, int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    b[idx]+=a[idx];
  }
}

__global__ void add_double(double* b,double* a, int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    b[idx]=a[idx]+a[idx];
  }
}

__global__ void mul_float(float* b,float* a, int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    b[idx]*=a[idx];
  }
}

__global__ void mul_double(double* b,double* a, int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    b[idx]*=a[idx];
  }
}

__global__ void divide_float(float* b,float* a, int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    b[idx]/=a[idx];
  }
}

__global__ void divide_double(double* b,double* a, int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    b[idx]/=a[idx];
  }
}

__global__ void add_scal_float(float* mat,float scal,int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    mat[idx]+=scal;
  }
}

__global__ void add_scal_double(double* mat,double scal,int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    mat[idx]+=scal;
  }
}

__global__ void mul_scal_float(float* mat,float scal,int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    mat[idx]*=scal;
  }
}

__global__ void mul_scal_double(double* mat,double scal,int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    mat[idx]*=scal;
  }
}

__global__ void pow_float(float* mat,float n,int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    mat[idx]=pow(mat[idx],n);
  }
}

__global__ void pow_double(double* mat,double n,int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if(idx<length)
  {
    mat[idx]=pow(mat[idx],n);
  }
}

__global__ void glfun(float* mat,int length)
{
  int idx=threadIdx.x + blockDim.x * blockIdx.x;
  if( (4*idx) <length)
  {
    mat[4*idx]+=1.0/100.0;
  }
}

}