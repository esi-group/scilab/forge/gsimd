/*
 * device.h
 *
 *  Created on: Jun 7, 2010
 *      Author: vlj
 */

#ifndef DEVICE_CUDA_HPP_
#define DEVICE_CUDA_HPP_


template<>
inline
Device<CUDAmode>::Device()
{

}

template<>
inline
Device<CUDAmode>::Device(Device_identifier ordinal)
{
	__check_sanity__<CUDAmode> ( cuDeviceGet ( &dev, ordinal ) );

	int major = 0, minor = 0;
	__check_sanity__<CUDAmode>( cuDeviceComputeCapability(&major,&minor,dev) );
	dev_cap = pair<int, int> (major, minor);

	__check_sanity__<CUDAmode>( cuDeviceTotalMem(&mem, dev) );

	int gpuOverlap;
	__check_sanity__<CUDAmode>( cuDeviceGetAttribute( &gpuOverlap, CU_DEVICE_ATTRIBUTE_GPU_OVERLAP, dev ) );
	support_cce = (gpuOverlap != 0);

	int canMapHostMemory;
	__check_sanity__<CUDAmode>( cuDeviceGetAttribute( &canMapHostMemory, CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY, dev ) );
	support_plm = (canMapHostMemory != 0);

	char deviceName[256];
	__check_sanity__<CUDAmode>( cuDeviceGetName(deviceName, 256, dev) );
	name = string(deviceName);
}




#endif /* DEVICE_H_ */
