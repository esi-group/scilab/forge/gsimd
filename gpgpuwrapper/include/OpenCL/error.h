/*
 * error.h
 *
 *  Created on: Jun 7, 2010
 *      Author: vlj
 */

#ifndef ERROR_OPENCL_HPP_
#define ERROR_OPENCL_HPP_

#include <exception>
#include <iostream>
using namespace std;


template<>
inline void Error::treat_error<OPENCLmode>(OPENCLmode::Status id)
{
	switch (id)
	{
	case CL_SUCCESS:
		return;
	case CL_DEVICE_NOT_FOUND:
		throw Error("Device Not Found");
	case CL_DEVICE_NOT_AVAILABLE:
		throw Error("Device Not Available");
	case CL_COMPILER_NOT_AVAILABLE:
		throw Error("Compiler Not Available");
	case CL_MEM_OBJECT_ALLOCATION_FAILURE:
		throw Error("Mem Object Allocation Failure");
	case CL_OUT_OF_RESOURCES:
		throw Error("Out Of Ressources");
	case CL_OUT_OF_HOST_MEMORY:
		throw Error("Out Of Host Memory");
	case CL_PROFILING_INFO_NOT_AVAILABLE:
		throw Error("Profiling Info Not Available");
	case CL_MEM_COPY_OVERLAP:
		throw Error("Mem Copy Overlap");
	case CL_IMAGE_FORMAT_MISMATCH:
		throw Error("Image Format Mismatch");
	case CL_IMAGE_FORMAT_NOT_SUPPORTED:
		throw Error("Image Format Not Supported");
	case CL_BUILD_PROGRAM_FAILURE:
		throw Error("Build Program Failure");
	case CL_MAP_FAILURE:
		throw Error("Map Failure");
	case CL_INVALID_VALUE:
		throw Error("Invalid Value");
	case CL_INVALID_DEVICE_TYPE:
		throw Error("Invalid Device Type");
	case CL_INVALID_PLATFORM:
		throw Error("Invalid Platform");
	case CL_INVALID_DEVICE:
		throw Error("Invalid Device");
	case CL_INVALID_CONTEXT:
		throw Error("Invalid Context");
	case CL_INVALID_QUEUE_PROPERTIES:
		throw Error("Invalid Queue Properties");
	case CL_INVALID_COMMAND_QUEUE:
		throw Error("Invalid Command Queue");
	case CL_INVALID_HOST_PTR:
		throw Error("Invalid Host Ptr");
	case CL_INVALID_MEM_OBJECT:
		throw Error("Invalid Mem Object");
	case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
		throw Error("Invalid Image Format Descriptor");
	case CL_INVALID_IMAGE_SIZE:
		throw Error("Invalid Image Size");
	case CL_INVALID_SAMPLER:
		throw Error("Invalid Sampler");
	case CL_INVALID_BINARY:
		throw Error("Invalid Binary");
	case CL_INVALID_BUILD_OPTIONS:
		throw Error("Invalid Build Options");
	case CL_INVALID_PROGRAM:
		throw Error("Invalid Program");
	case CL_INVALID_PROGRAM_EXECUTABLE:
		throw Error("Invalid Program Executable");
	case CL_INVALID_KERNEL_NAME:
		throw Error("Invalid Kernel Name");
	case CL_INVALID_KERNEL_DEFINITION:
		throw Error("Invalid Kernel Definition");
	case CL_INVALID_KERNEL:
		throw Error("Invalid Kernel");
	case CL_INVALID_ARG_INDEX:
		throw Error("Invalid Arg Index");
	case CL_INVALID_ARG_VALUE:
		throw Error("Invalid Arg Value");
	case CL_INVALID_KERNEL_ARGS:
		throw Error("Invalid Kernel Args");
	case CL_INVALID_WORK_DIMENSION:
		throw Error("Invalid Work Dimension");
	case CL_INVALID_WORK_GROUP_SIZE:
		throw Error("Invalid Work Group Size");
	case CL_INVALID_WORK_ITEM_SIZE:
		throw Error("Invalid Work Item Size");
	case CL_INVALID_GLOBAL_OFFSET:
		throw Error("Invalid Global Offset");
	case CL_INVALID_EVENT_WAIT_LIST:
		throw Error("Invalid Event Wait List");
	case CL_INVALID_EVENT:
		throw Error("Invalid Event");
	case CL_INVALID_OPERATION:
		throw Error("Invalid Operation");
	case CL_INVALID_GL_OBJECT:
		throw Error("Invalid GL Object");
	case CL_INVALID_BUFFER_SIZE:
		throw Error("Invalid Buffer Size");
	case CL_INVALID_MIP_LEVEL:
		throw Error("Invalid Mip Level");
	case CL_INVALID_GLOBAL_WORK_SIZE:
		throw Error("Invalid Global Work Size");
	default:
		break;
	}

}


#endif /* ERROR_H_ */
