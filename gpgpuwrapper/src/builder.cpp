/*
 * builder.cpp
 *
 *  Created on: 6 juin 2010
 *      Author: vlj
 */
#include <iostream>
#include <fstream>
#include <all_wrap.h>
using namespace std;



class Builder: public Context<OPENCLmode>
{
	typedef Context<OPENCLmode> Base;

public:

	string	build(string sourcefile, string buildoption)
	{
	  cl_int ciErrNum = CL_SUCCESS;
	  size_t num;
	  unsigned char* code = filetostr(sourcefile.c_str(), "", &num);

	  cl_program module = clCreateProgramWithSource(Base::cont, 1,
		  (const char**) &code, &num, &ciErrNum);
	  __check_sanity__<OPENCLmode>(ciErrNum);


	  cl_int status = clBuildProgram(module, 1, &(get_dev()), buildoption.c_str(), 0, 0);
	  if (status == CL_BUILD_PROGRAM_FAILURE)
		{
		  size_t sz;
		  __check_sanity__<OPENCLmode>( clGetProgramBuildInfo(module,get_dev(),CL_PROGRAM_BUILD_LOG,0,NULL,&sz) );
		  char* log = new char[sz + 1];
		  __check_sanity__<OPENCLmode>( clGetProgramBuildInfo(module,get_dev(),CL_PROGRAM_BUILD_LOG,sz,reinterpret_cast<void*>(log),NULL) );
		  string slog=string(log);
		  delete [] log;
		  throw Error (string("Build Failure :\n") + slog);
		}
	  size_t binlength;
	  __check_sanity__<OPENCLmode>(clGetProgramInfo(module,CL_PROGRAM_BINARY_SIZES,sizeof(size_t),&binlength,NULL));
	  unsigned char* bin=new unsigned char[binlength+1];
	  __check_sanity__<OPENCLmode>(clGetProgramInfo(module,CL_PROGRAM_BINARIES,binlength*sizeof(unsigned char),&bin,NULL));
	  bin[binlength]='\0';
	  string retvalue=string(reinterpret_cast<char*>(bin));
	  delete [] code;

	  return retvalue;
	}
};

int main(int argc, char** argv)
{
	if (argc < 2)
	{
		cout << "No source file" << endl;
		return 1;
	}

	Builder builder_context;
	builder_context.set_current_device<false>(builder_context.get_devices_list().at(0));

	try
	{
		 ofstream myfile;
		 string output=string(argv[1])+string(".out");
		  myfile.open (output.c_str());


		string str = builder_context.build(string(argv[1]), "-Werror");
		myfile << str;
		myfile.close();
	} catch (Error ex)
	{
		cout << ex.what();
	}


	return 0;
}
