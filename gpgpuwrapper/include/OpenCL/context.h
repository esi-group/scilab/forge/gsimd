/*!
 *  \file context.hpp
 *
 *  \date 24 juin 2010
 *  \author vlj
 */

#ifndef CONTEXT_OPENGL_HPP_
#define CONTEXT_OPENGL_HPP_

/*!
 * \cond
 */

#include <vector>
#include <map>
#include <GL/glx.h>
using namespace std;

template<>
  inline int
  Context<OPENCLmode>::number_of_device()
  {
    return -1;
  }

template<>
  inline
  Context<OPENCLmode>::Context()
  {

    cl_uint numplatforms;
    cl_uint devicescount = 0;
    __check_sanity__<OPENCLmode> (clGetPlatformIDs(0, NULL, &numplatforms));
    platforms = new cl_platform_id[numplatforms];
    __check_sanity__<OPENCLmode> (clGetPlatformIDs(numplatforms, platforms,
        NULL));
    Platform platform = platforms[0];


    __check_sanity__<OPENCLmode> (clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU,
        0, 0, &devicescount));
    cl_device_id* devices = new cl_device_id[devicescount];
    __check_sanity__<OPENCLmode> (clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU,
        devicescount, devices, 0));
    for (int k = 0; k < devicescount; ++k)
      {
        devices_list.push_back(Device<OPENCLmode> (devices[k]));
      }
    delete[] devices;

  }

template<>
  template<bool isGL>
    inline void
    Context<OPENCLmode>::set_current_device(const Device<OPENCLmode>& device)
    {
    current_device = device;
      if (isGL)
        {

    	  cl_int ciErrNum = CL_SUCCESS;
          cl_context_properties props[] =
          {
              CL_GL_CONTEXT_KHR, (cl_context_properties)glXGetCurrentContext(),
              CL_GLX_DISPLAY_KHR, (cl_context_properties)glXGetCurrentDisplay(),
              CL_CONTEXT_PLATFORM, (cl_context_properties) platforms[0],
              0
          };
          cont = clCreateContext(props, 1, &(current_device.dev), 0, 0, &ciErrNum);
          __check_sanity__<OPENCLmode> (ciErrNum);
        }
      else
        {

          cl_int ciErrNum = CL_SUCCESS;
          cont = clCreateContext(0, 1, &(current_device.dev), 0, 0, &ciErrNum);
          __check_sanity__<OPENCLmode> (ciErrNum);

        }
    }

template<>
  inline
  Context<OPENCLmode>::~Context()
  {
	delete [] platforms;
  }

/*!
 * \endcond
 */

#endif /* CONTEXT_HPP_ */
