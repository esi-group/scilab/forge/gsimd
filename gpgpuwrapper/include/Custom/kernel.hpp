/*
 * \file kernel.hpp
 *
 *  \date 21 juin 2010
 *  \author vlj
 */

#ifndef KERNEL_HPP_
#define KERNEL_HPP_


#define ALIGN_UP(off,alignment) \
  (off) = ( (off) +  ( alignment ) - 1 ) & ~ ( ( alignment ) - 1 );

/*!
 * This classe represents a kernel, that is a on GPU executed function.
 * It should not be created directly, but obtained by the getFunction Module's member.
 */
template<typename ModeDefinition>
class Kernel
{
	typedef typename ModeDefinition::Status Status;
	typedef typename ModeDefinition::Function_Handle Function_Handle;
	typedef typename ModeDefinition::Stream Stream;
protected:

	int offset;
	Function_Handle fonc;

public:
	Kernel();
	Kernel(Function_Handle fptr);

	/*!
	 * Pass a Matrix<T> as an argument.
	 */
	template<typename T>
	void pass_argument(shared_ptr<Matrix<ModeDefinition,T> > input);

	/*!
	 * Pass a GLMatrix as an argument.
	 * An OpenGL context must be set and active.
	 */
	template<typename T>
	void pass_argument(shared_ptr<GLMatrix<ModeDefinition, T> > input);

	/*!
	 * Pass a scalar T (int, float, double) as an input
	 */
	template<typename T>
	void pass_argument(T f);

	/*!
	 * Plan an execution of the kernel in the queue.
	 */
	void launch(Queue<ModeDefinition> queue,int xdim, int ydim, int grid_w, int grid_h);
};



#endif /* KERNEL_HPP_ */
