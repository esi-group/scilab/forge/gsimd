/*!
 * \file device.hpp
 *
 *  \date 21 juin 2010
 *  \author vlj
 */

#ifndef DEVICE_HPP_
#define DEVICE_HPP_


/*!
 * Device represents a GPGPU capable device present in the system.
 */
template<class ModeDefinition>
class Device
{
	enum
	{
		mode=ModeDefinition::mode
	};
	typedef typename ModeDefinition::Device_Handle Device_Handle;
	typedef typename ModeDefinition::Status Status;
	typedef typename ModeDefinition::Device_identifier Device_identifier;
	friend class Context<ModeDefinition> ;
protected:
	Device_Handle dev;
	pair<int, int> dev_cap;
	 size_t mem;
	bool support_plm;
	bool support_cce;
	string name;

	Device(Device_identifier ordinal);
	Device();
public:
	/*!
	 * Return the CUDA device_capability of the device (for instance to know if it can handle double).
	 * Does not currently work in OpenCL mode.
	 */
	pair<int, int>	device_capability() const;
	/*!
	 * Return total amount of dedicated memory on the device
	 */
	int	memory_amount() const;

	/*!
	 * Return the name of the device
	 */
	string	get_name() const;

	/*!
	 * True if the device does support page_locked_memory feature, also know as "pinned memory".
	 * Does not currently work in OpenCL mode.
	 */
	bool support_page_locked_memory() const;

	/*!
	 * True if the device does support concurrent copy and execution feature, also know as "overlapping".
	 * Does not currently work in OpenCL mode.
	 */
	bool support_concurrent_copy_execution() const;
};


/*!
 * \cond
 */
template<typename ModeDefinition>
inline
pair<int, int>	Device<ModeDefinition>::device_capability() const
{
	return dev_cap;
}

template<typename ModeDefinition>
inline
int	Device<ModeDefinition>::memory_amount() const
{
	return mem;
}

template<typename ModeDefinition>
inline
string	Device<ModeDefinition>::get_name() const
{
	return name;
}

template<typename ModeDefinition>
inline
bool Device<ModeDefinition>::support_page_locked_memory() const
{
	return support_plm;
}

template<typename ModeDefinition>
inline
bool Device<ModeDefinition>::support_concurrent_copy_execution() const
{
	return support_cce;
}

/*!
 * \endcond
 */


#endif /* DEVICE_HPP_ */
